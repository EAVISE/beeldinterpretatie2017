**sessie 0: inlezen beelden + opsplitsen kleurkanalen**

--> zie pdf introductieles

**sessie 1: manipuleren van pixel informatie in een beeld**

_Opdracht 1: Thresholding_
 * 1.1: Segmenteer de skin pixels van imageColor.png.
   * Gebruik hiervoor de skin color regels in het RGB domein : `(RED>95) && (GREEN>40) && (BLUE>20) && ((max(RED,max(GREEN,BLUE)) - min(RED, min(GREEN,BLUE)))>15) && (abs(RED-GREEN)>15) && (RED>GREEN) && (RED>BLUE);`
   * Visualiseer naast masker ook de resulterende pixel waarden.
 * 1.2: Segmenteer de tekst van de achtergrond in imageBimodal.png via OTSU thresholding.
   * Start met basis OTSU thresholding van de afbeelding. Wat gaat mis?
   * Hoe kan het originele grijswaarden beeld verbeterd worden om dit tegen te gaan?
        * Techniek 1: histogram equalization: `equalizeHist()`
        * Techniek 2: CLAHE: `Ptr operator = createCLAHE(); clahe_pointer->setTilesGridSize(Size(15,15)); clahe_pointer->setClipLimit(1);`
   * Problemen bij beide technieken?

_Opdracht 2: Erosie en dilatie_
 * 2.1: Baseer je op afbeelding imageColorAdapted.png en ga na wat verkeerd gaat als je deze afbeelding door bovenstaande pipeline haalt.
 * 2.2: Gebruik opening, closing, dilatie, erosie om het binaire masker proper te maken en ruis te onderdrukken
    * Probeer gescheiden ledematen terug te verbinden
    * Probeer niet volle binaire regios te vullen
 * 2.3: Probeer in resulterende blobs defecten weg te werken --> smooth edge blobs (TIP: convexity defects/convex hull)
    * Bruikbare functies: `erode()` / `dilate()` / `findContours()` / `convexHull()` / `drawContours()`

**sessie 2: kleurruimtes + segmentaties**

Omdat autonome wagens heel erg in de opmars zijn willen we dat deze wagens zich ook aan de verkeersregels houden zoals de maximaal toegelaten snelheid. Hiervoor gaan we een camera in de auto monteren en een computer die verkeersborden zal detecteren en analyseren. Jouw opdracht is een stukje software schrijven om de rode verkeersborden te filteren van de achtergrond zodat deze later kunnen geanalyseerd worden.

 * Opdracht 1: Segmenteer de verkeersborden in de BGR kleurenruimte
 * Opdracht 2: Segmenteer de verkeersborden in de HSV kleurenruimte
    * Gebruik hierbij trackbars om de optimale instellingen te bepalen
    * Bepaal beperkingen op H & S kanaal
 * Opdracht 3: Gebruik connected component analyse om enkel het stopbord over te houden

**sessie 3: template based matching**

Op een transportband komen zowel metalen plaatjes als metalen ringen voorbij (zie input foto). De metalen plaatjes moeten gedetecteerd worden en gelabeld als in de output foto. Je moet de plaatjes detecteren door gebruik te maken van template matching.

 * Opdracht 1: Gebruik template matching om een object te vinden in een inputbeeld
 * Opdracht 2: Pas de template matching aan om lokaal naar maxima te zoeken
 * EXTRA: Pas de template matching aan geroteerde objecten te vinden (roteren van beeld, rotatedRect, warpPerspective op hoekpunten)

**sessie 4: keypoint detection and matching**

In eye-tracker onderzoek moeten we dikwijls op zoek naar een op voorhand gekend object in het beeld. Template matching is hier een mogelijkheid, maar wanneer dit grandioos faalt, bestaan er meer robuustere technieken om dit aan te pakken. Keypoint detectie en matching is zo een aanpak.

 * Opdracht 1: Bepaal op uw input en template beeld keypoints met ORB, BRISK en AKAZE.
 * Opdracht 2: Kies 1 van bovenstaande keypoint detectoren, bepaal descriptoren van de keypoints en doe brute force matching.
 * Opdracht 3: Pas RANSAC en een homography toe om uw object terug te vinden.

**sessie 5: Machine learning voor het efficient scheiden van rijpe aardbeien in een plukrobot applicatie**

 * Opdracht 1: Maak een interface op basis van mouse callbacks die locaties van aangeklikte pixels opslaat (linker muis knop), toestaat om de lijst van punten weer te geven (midden muis knop) en de laatst toegevoegde punten te verwijderen (rechter muis knop).
 * Opdracht 2: Op basis van de geselecteerde punten bepaal je een descriptor, in dit geval de HSV representatie van de pixel.
 * Opdracht 3: Train op de geselecteerde descriptoren een K-Nearest-Neighbor classifier, een Normal Bayes classifier en een Support Vector Machine.
 * Opdracht 4: classificeer de pixels van de afbeelding met deze technieken en visualiseer masker + resterende pixels!
 * EXTRA: Hoe kan je de segmentatie verbeteren? Denk na over feit dat een rijpe aardbei meer rood en minder groen componenten heeft.

**sessie 6: Face and person detection + tracking by detection**

 * Opdracht 1: Implementeer een frontal face detector op basis van het Viola&Jones algorithme
    * 1.1 Doe dit eerst met HAAR-wavelet features
    * 1.2 Doe dit nadien met LBP features
    * 1.3 Haal naast de detectie (locatie) van het gezicht, ook een zekerheidsscore uit de detector
 * Opdracht 2: Pedestrian detectie via het HOG+SVM algorithme
    * 2.1 Implementeer een HOG+SVM pedestrian detector
    * 2.2 Hou per persoon een unieke tracker bij, via het tracking-by-detection principle

**sessie 7: Stereo calibratie en bepalen van diepte in stereo afbeeldingen**

 * Opdracht 1: lees 'imageleft.png' en 'imageright.png' in en visualiseer. Zorg ervoor dat je de x-coordinaat van een aangeklikte pixel in het linker beeld en die van een aangeklikte pixel in het rechter beeld kan opslaan.
 * Opdracht 2: lees 'intrinsics.yml' en 'extrinsics.yml' in en gebruik deze calibratie parameters om het linker beeld en het rechter beeld te calibreren. Voor het inlezen gebruik je 'cv::FileStorage' en voor de calibratie gebruik je 'cv::initUndistortRectifyMap' en 'cv::remap'. Vizualiseer de gecalibreerde beelden.
 * Opdracht 3: bepaal de diepte van het colablikje in centimeter a.h.v. twee aangeklikte pixel coordinaten. Gegeven:
    * focal length = 704 (in pixels)
    * baseline (afstand tussen de twee camera's) = 12.2 cm
 * Opdracht 4: haal 'focal length' en 'baseline' uit de gegeven calibratie parameters
 * Extra: Bepaal camera calibratie parameters a.h.v. de afbeeldingen in 'calibration/images'. Compileer hiervoor volgend programma: https://github.com/opencv/opencv/blob/master/samples/cpp/stereo_calib.cpp

--------------------------------------------

**EXAMEN - maandag 22/01/2018 - VM of NM**

Opsplitsing: halve dag theorie / halve dag laboefening

Benodigdheden:

 * Alle opgeloste oefeningen
 * Offline beschikbare HTML documentatie van OpenCV3
 * Alle documentatie die je zelf wil meebrengen (niet digitaal)

Geen internetverbinding beschikbaar

Kennis
 
*  We gaan ervan uit dat je alle opdrachten uit het labo beheerst
*  De EXTRA opdrachten zijn optioneel en kunnen je dat extra punt bezorgen

USB stick van collega = automatisch 0 voor vak

Zorg dat je de programmeer-richtlijnen van het vak volgt op het examen